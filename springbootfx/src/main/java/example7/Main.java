package example7;

import de.felixroske.jfxsupport.GUIState;
import de.felixroske.jfxsupport.SplashScreen;
import javafx.fxml.FXMLLoader;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;

import java.io.IOException;

@SpringBootApplication
public class Main extends AbstractJavaFxApplicationSupport{
    public static void main(String[] args) throws IOException {
        SplashScreen splashScreen = new SplashScreen() {
            public String getImagePath() {
                return "/images/数据库申请.png";
            }
        };
        launch(Main.class, HelloworldView.class, splashScreen, args);
    }
}
