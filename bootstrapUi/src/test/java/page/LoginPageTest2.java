package page;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ldh.common.ui.page.LoginPage;
import ldh.common.ui.page.LoginPage2;
import ldh.fx.component.LWindowBase;


public class LoginPageTest2 extends Application {

    public static Stage STAGE = null;

    public void start(Stage primaryStage) throws Exception {
        LoginPage2 loginPage2 = new LoginPage2(400, 380);
        LWindowBase lWindowBase = new LWindowBase();
        lWindowBase.setContentPane(loginPage2);
        lWindowBase.buildMovable(loginPage2);
        lWindowBase.buildResizable(loginPage2);
        loginPage2.setStage(primaryStage);
        Scene scene = new Scene(lWindowBase, 400, 380);
//        scene.getStylesheets().addAll("bootstrapfx.css");
        primaryStage.setScene(scene);
        scene.setFill(null);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.centerOnScreen();
        primaryStage.show();
        STAGE = primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
